package com.company;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class MainTest {
    @Test
    public void getStudentListByFacultyTest(){
        Student student1 = new Student(1, "2", "3", "4", 5, "6", "7", "8", "9", "10");
        Student student2 = new Student(11, "12", "13", "14", 15, "16", "17", "18", "19", "110");


        List<Student> expected = new ArrayList<>();
        List<Student> actual = new ArrayList<>();

        actual.add(student1);
        actual.add(student2);

        expected.add(student1);


        assertEquals(expected,Main.getStudentListByFaculty(actual,"8"));
    }

    @Test
    public void getStudentListByFacultyAndCourseTest(){
        Student student1 = new Student(1, "2", "3", "4", 5, "6", "7", "8", "9", "10");
        Student student2 = new Student(11, "12", "13", "14", 15, "16", "17", "18", "19", "110");


        List<Student> expected = new ArrayList<>();
        List<Student> actual = new ArrayList<>();

        actual.add(student1);
        actual.add(student2);

        expected.add(student1);


        assertEquals(expected,Main.getStudentListByFacultyAndCourse(actual,"8" , "9"));
    }

    @Test
    public void getStudentListByYearTest(){
        Student student1 = new Student(1, "2", "3", "4", 5, "6", "7", "8", "9", "10");
        Student student2 = new Student(11, "12", "13", "14", 15, "16", "17", "18", "19", "110");


        List<Student> expected = new ArrayList<>();
        List<Student> actual = new ArrayList<>();

        actual.add(student1);
        actual.add(student2);

        expected.add(student2);


        assertEquals(expected,Main.getStudentListByYear(actual,14));
    }

    @Test
    public void getStudentListByGroupTest(){
        Student student1 = new Student(1, "2", "3", "4", 5, "6", "7", "8", "9", "10");
        Student student2 = new Student(11, "12", "13", "14", 15, "16", "17", "18", "19", "110");



        List<Student> actual = new ArrayList<>();

        actual.add(student1);
        actual.add(student2);




        assertEquals( "[3 2 4, 13 12 14]",Main.getStudentListByGroup(actual).toString());
    }

    @Test
    public void getStudentListByGroup2Test(){
        Student student1 = new Student(1, "2", "3", "4", 5, "6", "7", "8", "9", "10");
        Student student2 = new Student(11, "12", "13", "14", 15, "16", "17", "18", "19", "110");



        List<Student> actual = new ArrayList<>();

        actual.add(student1);
        actual.add(student2);




        assertEquals( "[3 2 4-8 10, 13 12 14-18 110]",Main.getStudentListByGroup2(actual).toString());

    }

    @Test
    public void getStudentListByCountTest(){
        Student student1 = new Student(1, "2", "3", "4", 5, "6", "7", "8", "9", "10");
        Student student2 = new Student(11, "12", "13", "14", 15, "16", "17", "18", "19", "110");



        List<Student> actual = new ArrayList<>();

        actual.add(student1);
        actual.add(student2);




        assertEquals( 1,Main.getStudentListByCount(actual, "8"));
    }
}
