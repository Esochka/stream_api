package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {


    public static void main(String[] args) {
        Student student1 = new Student(1, "2", "3", "4", 5, "6", "7", "8", "9", "10");
        Student student2 = new Student(11, "12", "13", "14", 15, "16", "17", "18", "19", "110");

        List<Student> students = new ArrayList<>();
        students.add(student1);
        students.add(student2);


        System.out.println(getStudentListByFaculty(students, "8"));
        System.out.println(getStudentListByFacultyAndCourse(students, "18","19"));

        System.out.println(getStudentListByYear(students, 14));
        System.out.println(getStudentListByGroup(students));
        System.out.println(getStudentListByGroup2(students));
        System.out.println(getStudentListByCount(students , "8"));

    }


    public static List<Student> getStudentListByFaculty(List<Student> students, String faculty) {
        return students.stream().filter((student -> student.getFaculty().equals(faculty))).collect(Collectors.toList());
    }

    public static List<Student> getStudentListByFacultyAndCourse(List<Student> students, String faculty, String course) {
        return students.stream().filter((student -> student.getFaculty().equals(faculty) && student.getCourse().equals(course))).collect(Collectors.toList());
    }

    public static List<Student> getStudentListByYear(List<Student> students, int year) {
        return students.stream().filter((student -> student.getYearOfBirth()>year)).collect(Collectors.toList());
    }
    public static List<String> getStudentListByGroup(List<Student> students) {
        return students.stream().map(student -> student.getLastName()+" " + student.getFirstName() +" "+ student.getPatronymic() ).collect(Collectors.toList());
    }
    public static List<String> getStudentListByGroup2(List<Student> students) {
        return students.stream().map(student -> student.getLastName()+" " + student.getFirstName() +" "+ student.getPatronymic() +"-"+ student.getFaculty() +" "+ student.getGroup() ).collect(Collectors.toList());
    }

    public static long getStudentListByCount(List<Student> students, String faculty) {
        return students.stream().filter((student -> student.getFaculty().equals(faculty))).count();
    }
}

